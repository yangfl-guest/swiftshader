From 2c398b45395aef3785428d13178bcd5a50bfb903 Mon Sep 17 00:00:00 2001
From: yangfl <yangfl@users.noreply.github.com>
Date: Mon, 5 Apr 2021 01:05:30 +0800
Subject: [PATCH 4/8] Use system glslang

---
 CMakeLists.txt                           |  11 ++-
 src/Pipeline/SpirvShaderInstructions.inl |  14 +--
 tests/VulkanWrapper/CMakeLists.txt       |   5 -
 tests/VulkanWrapper/Util.cpp             | 115 ++++++++++++++++++++++-
 4 files changed, 122 insertions(+), 23 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index b2006eaed..b914f68e7 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -246,7 +246,15 @@ if (SWIFTSHADER_BUILD_TESTS OR SWIFTSHADER_BUILD_BENCHMARKS)
 endif()
 
 if (BUILD_VULKAN_WRAPPER)
-    InitSubmodule(glslang ${THIRD_PARTY_DIR}/glslang)
+    set(GLSLANG_TARGET_DIR "/usr/lib/x86_64-linux-gnu/cmake")
+    include("${GLSLANG_TARGET_DIR}/OSDependentTargets.cmake")
+    include("${GLSLANG_TARGET_DIR}/OGLCompilerTargets.cmake")
+    if(EXISTS "${GLSLANG_TARGET_DIR}/HLSLTargets.cmake")
+        # hlsl support can be optional
+        include("${GLSLANG_TARGET_DIR}/HLSLTargets.cmake")
+    endif()
+    include("${GLSLANG_TARGET_DIR}/glslangTargets.cmake")
+    include("${GLSLANG_TARGET_DIR}/SPIRVTargets.cmake")
 endif()
 
 if (SWIFTSHADER_BUILD_TESTS)
@@ -960,7 +968,6 @@ if(HAVE_PVR_SUBMODULE AND SWIFTSHADER_BUILD_PVR)
     set(PVR_TARGET_OTHER
         glslang
         glslangValidator
-        glslang-default-resource-limits
         OGLCompiler
         OSDependent
         OpenCLMatrixMultiplication
diff --git a/src/Pipeline/SpirvShaderInstructions.inl b/src/Pipeline/SpirvShaderInstructions.inl
index 8848b774e..ab31f9511 100644
--- a/src/Pipeline/SpirvShaderInstructions.inl
+++ b/src/Pipeline/SpirvShaderInstructions.inl
@@ -583,16 +583,4 @@ DECORATE_OP(T, OpConvertUToAccelerationStructureKHR)
 DECORATE_OP(T, OpIgnoreIntersectionKHR)
 DECORATE_OP(T, OpTerminateRayKHR)
 DECORATE_OP(T, OpTypeRayQueryKHR)
-DECORATE_OP(T, OpConstFunctionPointerINTEL)
-DECORATE_OP(T, OpAsmTargetINTEL)
-DECORATE_OP(T, OpAsmINTEL)
-DECORATE_OP(T, OpAsmCallINTEL)
-DECORATE_OP(T, OpVariableLengthArrayINTEL)
-DECORATE_OP(T, OpSaveMemoryINTEL)
-DECORATE_OP(T, OpRestoreMemoryINTEL)
-DECORATE_OP(T, OpPtrCastToCrossWorkgroupINTEL)
-DECORATE_OP(T, OpCrossWorkgroupCastToPtrINTEL)
-DECORATE_OP(T, OpTypeBufferSurfaceINTEL)
-DECORATE_OP(T, OpTypeStructContinuedINTEL)
-DECORATE_OP(T, OpConstantCompositeContinuedINTEL)
-DECORATE_OP(T, OpSpecConstantCompositeContinuedINTEL)
+DECORATE_OP(T, OpFunctionPointerINTEL)
diff --git a/tests/VulkanWrapper/CMakeLists.txt b/tests/VulkanWrapper/CMakeLists.txt
index 4cf4479e9..6773f6490 100644
--- a/tests/VulkanWrapper/CMakeLists.txt
+++ b/tests/VulkanWrapper/CMakeLists.txt
@@ -45,10 +45,6 @@ if (NOT TARGET glslang)
     message(FATAL_ERROR "Missing required target: glslang")
 endif()
 
-if (NOT TARGET glslang-default-resource-limits)
-    message(FATAL_ERROR "Missing required target: glslang-default-resource-limits")
-endif()
-
 if (NOT TARGET SPIRV)
     message(FATAL_ERROR "Missing required target: SPIRV")
 endif()
@@ -82,6 +78,5 @@ target_link_options(VulkanWrapper
 target_link_libraries(VulkanWrapper
     PUBLIC
         glslang
-        glslang-default-resource-limits
         SPIRV
 )
diff --git a/tests/VulkanWrapper/Util.cpp b/tests/VulkanWrapper/Util.cpp
index 452cb3955..7b712ee53 100644
--- a/tests/VulkanWrapper/Util.cpp
+++ b/tests/VulkanWrapper/Util.cpp
@@ -13,8 +13,8 @@
 // limitations under the License.
 
 #include "Util.hpp"
-#include "SPIRV/GlslangToSpv.h"
-#include "StandAlone/ResourceLimits.h"
+#include <glslang/SPIRV/GlslangToSpv.h>
+#include <glslang/Include/ResourceLimits.h>
 
 namespace Util {
 
@@ -151,7 +151,116 @@ std::vector<uint32_t> compileGLSLtoSPIRV(const char *glslSource, EShLanguage gls
 
 	const int defaultVersion = 100;
 	EShMessages messages = static_cast<EShMessages>(EShMessages::EShMsgDefault | EShMessages::EShMsgSpvRules | EShMessages::EShMsgVulkanRules);
-	bool parseResult = glslangShader->parse(&glslang::DefaultTBuiltInResource, defaultVersion, false, messages);
+
+	const TBuiltInResource DefaultTBuiltInResource = {
+		/* .MaxLights = */ 32,
+		/* .MaxClipPlanes = */ 6,
+		/* .MaxTextureUnits = */ 32,
+		/* .MaxTextureCoords = */ 32,
+		/* .MaxVertexAttribs = */ 64,
+		/* .MaxVertexUniformComponents = */ 4096,
+		/* .MaxVaryingFloats = */ 64,
+		/* .MaxVertexTextureImageUnits = */ 32,
+		/* .MaxCombinedTextureImageUnits = */ 80,
+		/* .MaxTextureImageUnits = */ 32,
+		/* .MaxFragmentUniformComponents = */ 4096,
+		/* .MaxDrawBuffers = */ 32,
+		/* .MaxVertexUniformVectors = */ 128,
+		/* .MaxVaryingVectors = */ 8,
+		/* .MaxFragmentUniformVectors = */ 16,
+		/* .MaxVertexOutputVectors = */ 16,
+		/* .MaxFragmentInputVectors = */ 15,
+		/* .MinProgramTexelOffset = */ -8,
+		/* .MaxProgramTexelOffset = */ 7,
+		/* .MaxClipDistances = */ 8,
+		/* .MaxComputeWorkGroupCountX = */ 65535,
+		/* .MaxComputeWorkGroupCountY = */ 65535,
+		/* .MaxComputeWorkGroupCountZ = */ 65535,
+		/* .MaxComputeWorkGroupSizeX = */ 1024,
+		/* .MaxComputeWorkGroupSizeY = */ 1024,
+		/* .MaxComputeWorkGroupSizeZ = */ 64,
+		/* .MaxComputeUniformComponents = */ 1024,
+		/* .MaxComputeTextureImageUnits = */ 16,
+		/* .MaxComputeImageUniforms = */ 8,
+		/* .MaxComputeAtomicCounters = */ 8,
+		/* .MaxComputeAtomicCounterBuffers = */ 1,
+		/* .MaxVaryingComponents = */ 60,
+		/* .MaxVertexOutputComponents = */ 64,
+		/* .MaxGeometryInputComponents = */ 64,
+		/* .MaxGeometryOutputComponents = */ 128,
+		/* .MaxFragmentInputComponents = */ 128,
+		/* .MaxImageUnits = */ 8,
+		/* .MaxCombinedImageUnitsAndFragmentOutputs = */ 8,
+		/* .MaxCombinedShaderOutputResources = */ 8,
+		/* .MaxImageSamples = */ 0,
+		/* .MaxVertexImageUniforms = */ 0,
+		/* .MaxTessControlImageUniforms = */ 0,
+		/* .MaxTessEvaluationImageUniforms = */ 0,
+		/* .MaxGeometryImageUniforms = */ 0,
+		/* .MaxFragmentImageUniforms = */ 8,
+		/* .MaxCombinedImageUniforms = */ 8,
+		/* .MaxGeometryTextureImageUnits = */ 16,
+		/* .MaxGeometryOutputVertices = */ 256,
+		/* .MaxGeometryTotalOutputComponents = */ 1024,
+		/* .MaxGeometryUniformComponents = */ 1024,
+		/* .MaxGeometryVaryingComponents = */ 64,
+		/* .MaxTessControlInputComponents = */ 128,
+		/* .MaxTessControlOutputComponents = */ 128,
+		/* .MaxTessControlTextureImageUnits = */ 16,
+		/* .MaxTessControlUniformComponents = */ 1024,
+		/* .MaxTessControlTotalOutputComponents = */ 4096,
+		/* .MaxTessEvaluationInputComponents = */ 128,
+		/* .MaxTessEvaluationOutputComponents = */ 128,
+		/* .MaxTessEvaluationTextureImageUnits = */ 16,
+		/* .MaxTessEvaluationUniformComponents = */ 1024,
+		/* .MaxTessPatchComponents = */ 120,
+		/* .MaxPatchVertices = */ 32,
+		/* .MaxTessGenLevel = */ 64,
+		/* .MaxViewports = */ 16,
+		/* .MaxVertexAtomicCounters = */ 0,
+		/* .MaxTessControlAtomicCounters = */ 0,
+		/* .MaxTessEvaluationAtomicCounters = */ 0,
+		/* .MaxGeometryAtomicCounters = */ 0,
+		/* .MaxFragmentAtomicCounters = */ 8,
+		/* .MaxCombinedAtomicCounters = */ 8,
+		/* .MaxAtomicCounterBindings = */ 1,
+		/* .MaxVertexAtomicCounterBuffers = */ 0,
+		/* .MaxTessControlAtomicCounterBuffers = */ 0,
+		/* .MaxTessEvaluationAtomicCounterBuffers = */ 0,
+		/* .MaxGeometryAtomicCounterBuffers = */ 0,
+		/* .MaxFragmentAtomicCounterBuffers = */ 1,
+		/* .MaxCombinedAtomicCounterBuffers = */ 1,
+		/* .MaxAtomicCounterBufferSize = */ 16384,
+		/* .MaxTransformFeedbackBuffers = */ 4,
+		/* .MaxTransformFeedbackInterleavedComponents = */ 64,
+		/* .MaxCullDistances = */ 8,
+		/* .MaxCombinedClipAndCullDistances = */ 8,
+		/* .MaxSamples = */ 4,
+		/* .maxMeshOutputVerticesNV = */ 256,
+		/* .maxMeshOutputPrimitivesNV = */ 512,
+		/* .maxMeshWorkGroupSizeX_NV = */ 32,
+		/* .maxMeshWorkGroupSizeY_NV = */ 1,
+		/* .maxMeshWorkGroupSizeZ_NV = */ 1,
+		/* .maxTaskWorkGroupSizeX_NV = */ 32,
+		/* .maxTaskWorkGroupSizeY_NV = */ 1,
+		/* .maxTaskWorkGroupSizeZ_NV = */ 1,
+		/* .maxMeshViewCountNV = */ 4,
+		/* .maxDualSourceDrawBuffersEXT = */ 1,
+
+		/* .limits = */ {
+			/* .nonInductiveForLoops = */ 1,
+			/* .whileLoops = */ 1,
+			/* .doWhileLoops = */ 1,
+			/* .generalUniformIndexing = */ 1,
+			/* .generalAttributeMatrixVectorIndexing = */ 1,
+			/* .generalVaryingIndexing = */ 1,
+			/* .generalSamplerIndexing = */ 1,
+			/* .generalVariableIndexing = */ 1,
+			/* .generalConstantMatrixVectorIndexing = */ 1,
+		}
+	};
+
+	bool parseResult = glslangShader->parse(&DefaultTBuiltInResource, defaultVersion, false, messages);
 
 	if(!parseResult)
 	{
-- 
2.30.2

